#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include "Data.h"
#include "Search_dis.h"
#include "Search_field.h"
#include "Search_doc.h"
#include "Search_med.h"

using namespace std;

int main()
{
    vector<int> choices;
    Data dta;
    dta.record_personal_data();
    system("cls");
    dta.show_menu();
    try{
    dta.choose(choices);
    if(choices.size()==0)
        throw "\n    Either you don't have any diseases or you have entered invalid choices.\n";
    }
    catch(const char* msg)
    {
        system("cls");
        system("color 03");
        printf(msg);
        return 0;
    }
    system("cls");
    dta.show_report();
    Search_dis a(choices,choices.size());
    Search_field b(a.get_out_list(),a.get_out_size());
    Search_doc c(b.get_out_list(),b.get_out_size());
    Search_med d(a.get_out_list(),a.get_out_size());
    for(int i=0;i<a.get_out_size();i++)
        cout<<a<<d<<c;
    system("color 03");
    dta.Show_warning();
    return 0;
}
