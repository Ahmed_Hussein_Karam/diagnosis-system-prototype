#ifndef SEARCH_DOC
#define SEARCH_DOC
#include <iostream>
#include <fstream>
#include <vector>
#include "Search.h"
#include "Dictionary_doc.h"

using namespace std;

class Search_doc: public Search
{
    private:
        Dictionary_doc translate_doc;

    public:
        Search_doc();
        Search_doc(vector <int> list, int inp_size );
        Search_doc(Search_doc &);
        Search_doc operator=(Search_doc &);
        void set_inp_size(int inp_size);
        void set_inp_list (vector<int> list);
        void read_list();
        void select_print_list() ;
        vector <int> get_out_list();
        ~Search_doc();
};

#endif // SEARCH_DOC
